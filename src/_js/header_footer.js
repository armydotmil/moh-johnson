// Last updated 2014-09-04 1710 BMN

var menu = {
	show_menu : function() {
		var window_width = parseInt($(window).width());
		var menu_width;
		
		if(menu.is_open)
		{
			menu_width = 66;
			menu.is_open = false;
		} else {
			menu_width = (window_width > 480) ? (window_width / 2) + 66 : window_width;
			menu.is_open = true;
		}
		$("#menu_button").animate({
			"right" : menu_width - 66
		}, {duration: 350, queue: false});
		$("#menu").animate({
			width: menu_width
		}, {duration: 350, queue: false});
	},

	bind_events : function() {
		//If menu is open and user clicks outside of the menu,
		//close the menu
		
		var click_handle = (Modernizr.touch) ? "click touchstart" : "click";
		
		$('html').on(click_handle, function() {
			if(menu.is_open)
			{
				menu.show_menu();
			}
		});

		//Unless click is inside menu
		$('#menu').on(click_handle, function(e) {
			e.stopPropagation();
		});

		$('.menu_section h4 a').on(click_handle, function(e) {
			var window_width = $(window).width();
			var off_set = 0;
			// add active class to parent .menu_section
			// but remove it from all others first
			$('.menu_section').removeClass('active');
			$(this).parents('.menu_section').addClass('active');
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				if (target.offset().top < position) {
					off_set = (window_width <= 1084) ? 66 : 50;
				}
				$('html,body').animate({
					scrollTop: target.offset().top - off_set // to compensate for the header bar
				}, 1000);
				if ($(window).width() <= 1084) {
					menu.show_menu();
				}
				return false;
			}
		});

		$("#menu_button").on(click_handle, function() {
			menu.show_menu();
			return false;
		});
		
		$(window).resize(function() {
			if ($(this).width() > 1084) {
				$("#menu").css({
					"height": "",
					"width" : ""
				});
				menu.is_open = false;
			}
		});
	}
};

var footer = {
	bind_events : function() {
		$('.footerheading').on('click', function() {
			if ($(this).find("span").is(":visible")) {
				var expand = $(this).find('.expand');
				var lists = $(this).parent().siblings();
				var next_list = $(this).next('ul');
				if (next_list.hasClass("active")) {
					next_list.removeClass("active");
					expand.text('+');
				} else {
					lists.find('.expand').text('+');
					lists.find("ul").removeClass("active");
					next_list.addClass("active");
					expand.text('-');
				}
			}
		});
	}
};

function get_new_img(img_size) {
	var imgs = [$('#biography_portrait'), $('#cyclorama'), $('#battle_image')],
	img_folders = ['biography', 'cyclorama', 'battle'],
	path = '/e2/rv5_images/medalofhonor/johnson/';
	
	// load the correct image sizes for the right column images
	for (var i = 0; i < imgs.length; i++) {
		imgs[i].prop({
			"src" : path + img_folders[i] + "/" + img_size + ".jpg"
		});
	}
}

var position = $(window).scrollTop();
$(window).scroll(function() {
	var collapse_point = 30,
	scroll_top = $(window).scrollTop(),
	buffer = $(".masthead_buffer").height(),
	divs = [$('#biography'), $('#narrative'), $('#battle'), $('#army_news')];

	if (scroll_top > collapse_point) {
		$("#top_bar,#menu").addClass("collapsed");
		$("#featureBar").addClass("fb_collapsed");
	} else {
		$("#top_bar,#menu").removeClass("collapsed");
		$("#featureBar").removeClass("fb_collapsed");
	}
	
	if (scroll_top > position && scroll_top > buffer && $(window).width() < 1085) {
		// down
		$("#top_bar,#menu").hide();
	} else {
		// up
		$("#top_bar,#menu").show();
	}
	position = scroll_top;
	
	/* commented out for now - may use in the future
	for (var i = 0; i < divs.length; i++) {
		var offset = ($(window).width() <= 1084) ? 0 : 50;
		
		if ($(window).scrollTop() >= divs[i].offset().top - offset) {
			$('.menu_section').removeClass('active');
			$('.menu_section').eq(i).addClass('active');
		}
	}*/
})
.resize(function() {
	var need_new_image = false,
	img_size,
	set_src = $('#biography_portrait').prop("src"),
	window_width = $(window).width();
	
	if (window_width > 768 && !set_src.match('373')) {
		need_new_image = true;
		img_size = '373'
	} else if (window_width <= 768 && window_width > 480 && !set_src.match('300')) {
		need_new_image = true;
		img_size = '300';
	} else if (window_width < 480 && !set_src.match('460')) {
		need_new_image = true;
		img_size = '460';
	}
	
	// only change the image if we need to
	if (need_new_image) {
		get_new_img(img_size);
	}
	
});

$(document).ready(function() {
	var window_width = $(window).width(),
	img_size = (window_width > 768) ? '373' : (window_width <= 768 && window_width > 480) ? '300' : '460';
	
	menu.bind_events();
	footer.bind_events();
	
	get_new_img(img_size);
	
	if (position > 30) {
		$("#top_bar,#menu").addClass("collapsed");
		$("#featureBar").addClass("fb_collapsed");
	}

	//Detect high res displays (Retina, HiDPI, etc...)
	Modernizr.addTest('highresdisplay', function(){
		if (window.matchMedia) {
			var mq = window.matchMedia("only screen and (-moz-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3), only screen and (min-resolution: 1.3dppx)");
			if(mq && mq.matches) {
				return true;
			}
	    }
	});
	
    new CeremonyImages().onClick();
    
	$("#official_citation").click(function(e) {
		e.preventDefault();
		$.get("citation.html", function(data){
			$("html").css("overflow-y", "hidden");

			$("#page").append(data);
	
			$("#lightbox").focus();
			
			$("#lightbox_outter_content").click(function(e) {
				e.stopPropagation();
			});
	
			$("#lightbox,.lightbox_button").click(function() {
				$("#lightbox").remove();
				$("html").css("overflow-y", "scroll");
			});
		});
		
	});
	
	 if(typeof addthis !== "undefined"){
		addthis.layers({
			'theme' : 'transparent',
			'share' : {
				'position' : 'left',
				'services' : 'facebook,twitter,google_plusone_share,pinterest',
				'offset': {
					'top':'230px'
				}
			},
			'thankyou' : 'false',
			'responsive' : {
				'maxWidth' : '768px'
			},
			 'mobile' : {
				'buttonBarPosition' : 'bottom',
				'buttonBarTheme' : 'light',
				'mobile' : true
			}
		});
	}
	

	//send an event to GA when a link is clicked
	$("a").on('click', function(event) {
		if ($(this).attr('href').match(/johnson/g)
		|| $(this).attr('href').match(/#/g)
		&& $(this).attr('href') !== "#") {
			var dest_uri = $(this).attr('href').split('/');
			var current_uri = window.location.pathname.split('/');
			var i = dest_uri.length - 1;
			var j = current_uri.length - 1;

			var dest_pagename = dest_uri[i - 1];
			var current_pagename = current_uri[j - 1];
			var section_name = current_pagename;
			var send_event = true;

			if ($.inArray('#', dest_uri[i]) > -1) {
				dest_pagename = dest_uri[i].split('#')[1];
			}

			if (current_pagename.match(/.html/g)) {
				current_pagename = current_uri[j - 2];
			}

			if ($(this).parents().hasClass('menu_section')) {
				section_name = 'menu';
				if($(this).find('span').length && menu.is_open) { // clicking on menu heading when mobile menu is open
					send_event = false;
				}
			}

			var description = current_pagename + "_" + dest_pagename;

			if (send_event && typeof ga === "function") {
				ga('send', 'event', section_name, 'click', description);
			}

		}
	});

});