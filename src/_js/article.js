var keyword = 'target_moh_johnson';

var Article = Backbone.Model.extend({
	urlRoot: 'https://api.army.mil/api/packages/getpackagesbykeywords?keywords=' + keyword
});

var Articles = Backbone.Collection.extend({
	model: Article,
	url: 'https://api.army.mil/api/packages/getpackagesbykeywords?keywords=' + keyword
});

var articles = new Articles();

var ArticleGallery = Backbone.View.extend({
	initialize: function(options) {
		for (var i = 0; i < options.rows; i++) {
			this.listenTo(articles, 'sync', this.load_articles);
		}
		articles.url += "&offset=" + options.offset + "&count=" + options.count;
		articles.fetch();
	},
	articles_loaded: 0,
	load_articles: function() {
		var news_stories = $('<div>').prop({
			'class': 'news_stories'
		});

		var num_articles = 2;

		for (var i = 0; i < num_articles; i++) {
			if (articles.length > 0) {
				news_stories.append(build_news_cell(articles.shift()));
			} else {
				$("#army_news .more_news_wrap").hide();
			}
		}
		$("#news_section .more_news_wrap").before(news_stories);
		$(".news_story").last().addClass("last_news_story");
	}
});

function build_news_cell(article) {
	var opts = {
		lines: 13, // The number of lines to draw
		length: 20, // The length of each line
		width: 10, // The line thickness
		radius: 30, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		color: '#000', // #rgb or #rrggbb or array of colors
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2, // The z-index (defaults to 2000000000)
		top: '50%', // Top position relative to parent
		left: '50%' // Left position relative to parent
	};
	var spinner = new Spinner(opts).spin();
	// refer to the spinner with: target.append(spinner.el);
	// then stop with spinner.stop();
	
	var page = $('body').prop('id');
	
	var window_width = parseInt($(window).width());
	
	var news_story = $('<div>').prop({
		'class': 'news_story'
	}).height(300);
	var max_index = article.get("images").length - 1;
	var random_index = Math.floor((Math.random() * max_index) + 0);
	var image = "";
	if(window_width < 769) {
		image = article.get("images")[random_index].url_size1;
	} else {
		image = article.get("images")[random_index].url_size2;
	}
	var alt = article.get("images")[random_index].alt;
	var title = article.get("title");
	
	title = (title.length > 50) ? title.substring(0, 50) + "..." : title;
	
	var url = article.get("page_url");
	var description = article.get("description").substring(0, 100) + "...";
	
	var image_url = url + '?from=moh_' + page + '_news_image';
	var title_url = url + '?from=moh_' + page + '_news_text';
	
	var news_story_image = $('<a>').prop({'href': image_url, "target": "_blank"});
	news_story.prepend(spinner.el);
	
	$('<img>').load(function() {
		news_story_image.append($(this));
		news_story.height('');
		spinner.stop();
	}).prop({
		'src': image,
		'alt': alt
	});
	var news_story_title = $('<p>').prop({
		'class': ''
	}).append($('<a>').prop({
		'class': 'news_story_title',
		'href': title_url
	}).html(title));

	return news_story.append(news_story_image).append(news_story_title);
}

$(document).ready(function() {
	
});