// this code requires jQuery, youtube_iframe_api.js, and spin_min.js
/*global $, YT, navigator, document, window, Spinner*/
// Spinner configuration from js/spin_min include
var opts = {
    lines: 13, // The number of lines to draw
    length: 20, // The length of each line
    width: 10, // The line thickness
    radius: 30, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#000', // #rgb or #rrggbb or array of colors
    speed: 1, // Rounds per second
    trail: 60, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    className: 'spinner', // The CSS class to assign to the spinner
    zIndex: 2, // The z-index (defaults to 2000000000)
    top: '50%', // Top position relative to parent
    left: '50%' // Left position relative to parent
};

var spinner = [];

// $(document).ready(function(){
//   var target= $('.ytHybridPlayer_<?php echo $this->version;?>_overlay');
//   target.each(function(i){
//     spinner[i]= new Spinner(opts);
//     spinner[i].spin($(this)[0]);
//     $(this).css('opacity', '0.7');
//   });
// });

function ytResponsivePlayer(playlist_id) {
    var player,
        playerLoaded = false,
        transitioning = false,
        context = this;
    // playAttempt = 0,
    // listAttempt = 0;

    this.initPlayer = function() {
        player = new YT.Player('player', {
            height: '480',
            width: '853',
            playerVars: {
                listType: 'playlist',
                list: playlist_id,
                rel: 0,
                showinfo: 0,
                autohide: 0,
                wmode: 'transparent',
                enablejsapi: 1
            },
            events: {
                'onReady': this.onPlayerReady,
                'onStateChange': this.onPlayerStateChange
            }
        });
        // player = new YT.Player('player', {
        //   events: {
        //     'onReady': this.onPlayerReady,
        //     'onStateChange': this.onPlayerStateChange
        //   }
        // });
    };
    this.initForMobile = function() {
        spinner.stop();
        $('#ytOverlay').fadeOut(1000);
        var iOS =
            (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false);
        if (!iOS) {
            $('.ytPlaylist').fadeIn(1000);

            $('.ytPlaylist .PlaylistInfo').click(function() {
                player.playVideo();
                spinner.spin(document.getElementById('ytOverlay'));
                $('#ytOverlay').fadeIn(1000);
                var showlist = $(this).closest('.ytPlaylist');
                if (showlist.find('ul').is(':hidden')) {
                    showlist.addClass('show');
                }
            });
        }
    };
    this.prevVideo = function() {
        player.previousVideo();
    };
    this.nextVideo = function() {
        player.nextVideo();
    };
    /*this.playVideo = function(){
    if(typeof player.playVideo == 'function'){
    alert('playVideo is good');
    player.playVideo();
    }else{
    alert('playVideo is not good');
    }
    };*/
    this.onPlayerReady = function() {
        // e.target.playVideo();

        if (context.mobilecheck()) {
            context.initForMobile();
        } else {
            player.playVideo();
        }
    };
    this.mobilecheck = function() {
        var check = false;
        /**
         * iOS is causing most of the issues,
         * so needs to be included in mobile devices check
         */
        if (navigator.userAgent.match(/(iPad|iPhone|iPod)/g)) return true;

        (function(a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
        })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
    };
    this.onPlayerStateChange = function(e) {
        // console.log(e.data);

        if (e.data == YT.PlayerState.PLAYING) {
            if (!playerLoaded) {
                // .getPlaylist only works once the video starts playing,
                // so it requires starting the video and then pause it
                if (!context.mobilecheck()) e.target.pauseVideo();
                playerLoaded = true;
                var videolist = e.target.getPlaylist();
                // alert(videolist.join(','));
                context.populatePlaylist(videolist.join(','));
            }
            if (transitioning) {
                context.setCurrentPlaying();
                transitioning = false;
            }
            $('.current').addClass('playing');
        } else {
            $('.current').removeClass('playing');
        }
        if (e.data == -1) {
            transitioning = true;
            $('.current').removeClass('playing');
        }
    };
    this.getParameterByName = function(url, name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
            results = regex.exec(url);
        return !results ? '' :
            decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
    this.populatePlaylist = function(videolist) {
        var playlistid = this.getParameterByName(
            $('#player').attr('src'), 'list'
        ),
            ajaxurl = '',
            ajaxdata = '';
        if (typeof videolist == 'undefined') {
            /*ajaxurl = "/api/youtube/listplaylistitems";
            ajaxdata = "playlistid="+playlistid+"&part=snippet,contentDetails";*/
            ajaxurl = 'https://api.army.mil/api/youtube/ListPlaylistItems?' +
            'playlistid=' + playlistid + '&part=snippet,contentDetails';
        } else {
            /*ajaxurl = "https://www.googleapis.com/youtube/v3/videos";
            ajaxdata = "id="+videolist+"&part=snippet,contentDetails&key=AIzaSyC-R9l5CR9SkQClgciZRecSctY_VI4GohQ";*/
            ajaxurl = 'https://api.army.mil/api/v1/youtube/videos?id=' +
                videolist + '&part=snippet,contentDetails';
        }
        $.ajax({
            url: ajaxurl,
            data: ajaxdata,
            dataType: 'json',
            success: function(data) {
                if (typeof data.items != 'undefined') {
                    var items = data.items;
                    var curpos = player.getPlaylistIndex();
                    var urlRegex = /(https?:\/\/[^\s]+)/g;
                    for (var i = 0; i < items.length; i++) {
                        var video = items[i].snippet,
                            currentitem = (curpos == i) ? ' current' : '',
                            newli = $('<li>').prop({
                                'id': 'vidIndex' + i,
                                'class': 'video_item' + currentitem
                            }),
                            vpos = $('<span>').prop({
                                'class': 'v_position'
                            }).text(i + 1),
                            vsym = $('<span>').prop({
                                'class': 'v_playsymbol'
                            }),
                            vtit = $('<span>').prop({
                                'class': 'v_title'
                            }).text(video.title),
                            vdur = $('<span>').prop({
                                'class': 'v_duration'
                            }),
                            vdl = $('<a>').prop({
                                'class': 'v_download'
                            }),
                            clearboth = $('<div>').prop({
                                'class': 'clearboth'
                            }),
                            duration = '',
                            description = video.description;

                        if (typeof items[i].contentDetails.duration !==
                            'undefined')
                            duration = items[i].contentDetails.duration;
                        duration = context.convert_time(duration);
                        vdur.text(duration);

                        //put together all the <span>s inside the new <li>
                        newli.append(vpos)
                            .append(vsym)
                            .append(vtit.append(vdur));

                        // if description has a URL, add it to the <li>
                        if (description.match(urlRegex)) {
                            vdl.prop({
                                'href': description.match(urlRegex)[0],
                                'target': '_blank'
                            });
                            newli.append(vdl);
                        }

                        newli.append(clearboth);

                        //append the <li> to the <ul>
                        $('.ytPlaylist .videos_list').append(newli);
                    }

                    spinner.stop();
                    $('.ytPlaylist').fadeIn(1000);
                    $('#ytOverlay').fadeOut(1000);

                    $('.ytPlaylist .PlaylistInfo .controls .nav').append(
                        $('<a>').prop({
                            'class': 'prev',
                            'title': 'previous video'
                        })
                    ).append(
                        $('<a>').prop({
                            'class': 'next',
                            'title': 'next video'
                        })
                    );

                    $('.ytPlaylist .PlaylistInfo .controls .pos').text(
                        (curpos + 1) + '/' + items.length + ' videos'
                    );

                    context.initializeLinks();
                    
                    $("a.v_download").on('click', function(event) {
                      var link = $(this).attr('href'),
                          filename = link.split('/').pop();
                      ga('send', 'event', filename, 'mp4', link);
                    });
                }
            }
        });
    };
    this.convert_time = function(duration) {
        var startpos = 2, // start after 'PT' part of string
            endpos = 0,
            h = '',
            m = '0',
            s = '00';

        if (duration.indexOf('H') != -1) {
            endpos = duration.indexOf('H');
            h = duration.substr(startpos, (endpos - startpos));
            startpos = endpos + 1; // set position after 'H'
        }

        if (duration.indexOf('M') != -1) {
            endpos = duration.indexOf('M');
            m = duration.substr(startpos, (endpos - startpos));
            startpos = endpos + 1; // set position after 'M'
        }

        // only include leading zero if h is set
        if (m.length == 1 && h !== '') m = '0' + m;

        if (duration.indexOf('S') != -1) {
            endpos = duration.indexOf('S');
            s = duration.substr(startpos, (endpos - startpos));
            if (s.length == 1) s = '0' + s;
        }

        if (h !== '') {
            return h + ':' + m + ':' + s;
        } else {
            return m + ':' + s;
        }
    };
    this.initializeLinks = function() {
        var links = $('.videos_list .video_item');
        links.click(function(e) {
            if (e.target.tagName == 'A') return true;
            var index = $(this).attr('id').substr(8);
            if (index != player.getPlaylistIndex()) {
                player.playVideoAt(index);
                spinner.spin(document.getElementsByClassName('ytPlaylist')[0]);
                $('#playlistOverlay').fadeIn(1000);
            }
        });// .hover(function() {
            // $(this).addClass('hover');
        // }, function() {
            // $(this).removeClass('hover');
        // });
        $('.ytPlaylist .nav .prev').click(function() {
            if (player.getPlaylistIndex() !== 0) {
                context.prevVideo();
                spinner.spin(document.getElementById('playlistOverlay'));
                $('#playlistOverlay').fadeIn(1000);
            }
        });
        $('.ytPlaylist .nav .next').click(function() {
            if ((player.getPlaylistIndex() + 1) <
                $(this).closest('.ytPlaylist').find('ul li').length) { // ?
                context.nextVideo();
                spinner.spin(document.getElementById('playlistOverlay'));
                $('#playlistOverlay').fadeIn(1000);
            }
        });
        $('.ytPlaylist .PlaylistInfo').unbind('click').click(function() {
            var showlist = $(this).closest('.ytPlaylist');
            if (showlist.find('ul').is(':hidden')) {
                showlist.addClass('show');
            } else {
                showlist.removeClass('show');
            }
        });
    };
    this.setCurrentPlaying = function() {
        spinner.stop();
        $('#playlistOverlay').fadeOut(1000);
        var newCurrent = player.getPlaylistIndex();
        var total = $('.videos_list .video_item').length;
        $('.videos_list .video_item').removeClass('current');
        $('#vidIndex' + newCurrent).addClass('current');
        //var videoTitle = $('#vidIndex'+newCurrent+' .v_title').text();

        $('.ytPlaylist .PlaylistInfo .controls .pos').text(
            (newCurrent + 1) + '/' + total + ' videos'
        );
    };
}
var yt_docready = 0;
var player_obj;
var spinner = new Spinner(opts);

function startYTAPI() {
    if (yt_docready == 2) {
        player_obj.initPlayer();
    }
}

function onYouTubeIframeAPIReady() {
    yt_docready++;
    startYTAPI();
    //console.log('iframe api ready!');
}
$(document).ready(function() {
    var yt = $('.ytContainer:first');
    if (yt.length > 0 && typeof yt.attr('id') != 'undefined' &&
        typeof yt.attr('title') != 'undefined') {
        var id = yt.attr('id'),
            title = yt.attr('title'),
            ytoverlay = $('<div>').prop({
                'id': 'ytOverlay'
            }),
            ytplayer = $('<div>').prop({
                'class': 'ytPlayer'
            }).append($('<div>').prop({
                'class': 'iframe_embed_container'
            }).append($('<div>').prop({
                'id': 'player'
            })));
        if (title.indexOf(' - ') != -1) {
            var dashpos = title.indexOf(' - ');
            var newtitle = title.substr(0, dashpos + 3);
            newtitle += "<span class='subhead'>";
            newtitle += title.substr(dashpos + 3);
            newtitle += '</span>';
            title = newtitle;
        }
        var ytplaylist = $('<div>').prop({
            'class': 'ytPlaylist'
        }).append($('<div>').prop({
            'id': 'playlistOverlay'
        })).append($('<div>').prop({
            'class': 'PlaylistInfo'
        }).append($('<h2>').html(title)).append($('<div>').prop({
            'class': 'controls'
        }).append($('<div>').prop({
            'class': 'nav'
        })).append($('<div>').prop({
            'class': 'pos'
        }))).append($('<div>').prop({
            'class': 'expand'
        }))).append($('<ul>').prop({
            'class': 'videos_list'
        }));

        player_obj = new ytResponsivePlayer(id);
        yt.removeAttr('id').removeAttr('title');
        yt.html('')
            .append(ytoverlay)
            .append(ytplayer)
            .append(ytplaylist)
            .append(
                $('<div>').prop({
                    'class': 'clearboth'
                })
            );

        spinner.spin(document.getElementById('ytOverlay'));

        $('#battle').waypoint(function() {
            yt_docready++;
            startYTAPI();
        }, {
            offset: 'bottom-in-view'
        });
    }
});
