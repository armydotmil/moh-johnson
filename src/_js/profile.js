;


function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

$(document).ready(function(){

	//0000 of the day of the ceremony (ET)
	var day_of_wh = 1433217600000;
	
	//One hour before WH scheduled start time
	var wh_start_time = 1433253600000;  // 11/06/14 1045 am
	//one hour after WH scheduled end time
	var wh_end_time = 1433260800000;  // 11/06/14 1245 pm
	
	var day_of_hoh = 1433304000000;
	
	//One hour before HOH scheduled start time
	var hoh_start_time = 1433336400000;
	//One hour after HOH scheduled end time
	var hoh_end_time = 1433343600000;
	
	var now = new Date();
	now = now.getTime();
		
	var override = GetURLParameter("time");
	override = parseInt(override);
		
	if(!isNaN(override)){
		now = override;
	}
	
	if(now > day_of_wh && now < wh_start_time) {
		stream_ad.init(
			"Live Webcast: Medal of Honor Ceremony",
            "Watch the Medal of Honor ceremony live webcast today at 11 a.m. EDT",
            "President Barack Obama will posthumously award the Medal of Honor to Sgt. Henry Johnson and Sgt. William Shemin today, at the White House. Johnson and Shemin will be awarded the Medal of Honor for their actions in World War I.",
            "/e2/rv5_images/medalofhonor/johnson/stream_ads/white_house.jpg",
			"The White House"
		);
	} else if(now > wh_start_time && now < wh_end_time) {
		$(".masthead_container:first").hide();
		live_stream.init(
			"Live Webcast: Medal of Honor Ceremony",
			1 // pass 1 to use wh live stream
		);
	} else if(now > wh_end_time && now < hoh_start_time) {
		var day_of = (now < day_of_hoh) ? 'tomorrow' : 'today';
		// test day_of with time = wh_end_time + 1 to see 'tomorrow'
		// test day_of with time = day_of_hoh to see 'today'
		stream_ad.init(
			"Live Webcast: Hall of Heroes Induction",
            "Watch the Hall of Heroes Induction Ceremony live webcast, " + day_of + " at 10 a.m. EDT",
    		"The U.S. Army will induct Sgt. Henry Johnson and Sgt. William Shemin into the Pentagon's Hall of Heroes today. The Pentagon ceremony will add Johnson’s and Shemin’s names to the distinguished roster in the Hall of Heroes, the Defense Department's permanent display of record for all recipients of the Medal of Honor.",
            "/e2/rv5_images/medalofhonor/johnson/stream_ads/hall_of_heroes.jpg",
			"The Pentagon"
		);
	} else if(now > hoh_start_time && now < hoh_end_time) {
		$(".masthead_container:first").hide();
		live_stream.init(
			"Live Webcast: Hall of Heroes Induction",
			6439
		);
	}
});

var stream_ad = {
	
	// test WH ceremony with (day of): 1415250000001
	// test HOH ceremony with (day of): 1415336400000
	
	init : function(maintitle, subtitle, message, image, alt) {
		
		var stream_ad = $("#stream_ad");
		stream_ad.show(); // since it is hidden by default
		stream_ad.find(".section_title h2").text(maintitle);
		
		var stream_info = $("#stream_info");
		
		var stream_info_left = $("<div>").prop({
			'id' : 'stream_info_left'
		});
		
		stream_info_left.append(
			$("<blockquote>").text(subtitle)
		);
		
		stream_info_left.append(
			$("<p>").text(message)
		);
		
		var stream_info_right = $("<div>").prop({
			'id' : 'stream_info_right'
		});
		
		stream_info_right.append(
			$("<img>").prop({
				"src" : image,
				"alt" : alt
			})
		);
		
		stream_info.append(stream_info_left).append(stream_info_right);
	}
};

var live_stream = {
	
	// test live stream WH with: 1415288700001
	// test live stream HOH with: 1415379600001
	
	init: function(maintitle, stream_id){
					
		var player_w = "",
		player_h = "",
		feed_width;
		
		$("#stream_ad").show();
	
		$("#stream_ad .section_title h2").text(maintitle);
		
		if(stream_id !== 1) {
			$("#stream_info").html("").append(
				$("<div>").prop({
					"id" : "dvids_ad"
				}).append(
					$("<div>").prop({
						"id" : "dvids_inner"
					})
				)
			);
			$("#stream_info").prepend(
				$("<div>").prop({
					"id" : "live_stream_feed"
				})
			);
			
			feed_width = parseInt($("#live_stream_feed").width());
		} else {
			$("#stream_info").prepend(
				$("<div>").prop({
					"id" : "yt_live_stream_feed"
				}).append(
					$("<div>").prop({
						"class" : "inner_container"
					}).append(
						$("<div>").prop({
							"class" : "player"
						})
					)
				)
			);
		}
		
		player_w = feed_width;
		if(player_w > 720) {
			player_w = 720;
		}
		player_h = Math.floor(player_w * 9 / 16);
		
		if (stream_id !== 1) {
			$("#live_stream_feed").append(
				'<iframe width="'+player_w+'" height="'+player_h+'" scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: '+player_w+'px; height: '+player_h+'px;" allowtransparency="true" src="http://www.dvidshub.net/webcast/embed/'+stream_id+'?show_description=0&width='+player_w+'&height='+player_h+'"></iframe>'
			);
			
			$("#dvids_inner").append(
				$("<p>").html("VIDEO PROVIDED BY <a href='http://www.dvidshub.net/'><img src='/e2/rv5_images/medalofhonor/valor24/dvids_logo.png' alt='dvids' /></a>")
			);
		} else {
			$("#yt_live_stream_feed .player").append(
				'<iframe width="100%" height="100%" src="//www.youtube.com/embed/EusxvUrgKQg" frameborder="0" allowfullscreen></iframe>'
			);
		}
	}
}



